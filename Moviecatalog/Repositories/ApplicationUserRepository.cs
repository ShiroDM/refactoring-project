﻿using Moviecatalog.Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Moviecatalog.Repositories
{
    public class ApplicationUserRepository : IApplicationUserRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public IEnumerable<ApplicationUser> GetAll()
        {
            return db.Users;
        }
        public ApplicationUser Get(string id)
        {
            return db.Users.First(o => o.Id == id);
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
        }
    }
}