﻿using Moviecatalog.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Moviecatalog.Repositories
{
    public class MovieRepository: IMoviePagesRepository
    {
       private MovieCatalogEntities db = new MovieCatalogEntities();

       public IEnumerable<Movies> GetAll()
       {
            return db.Movies.AsNoTracking();
       }

        public Movies Get(Guid? id)
       {
           return db.Movies.AsNoTracking().First(o => o.Id == id);
       }

       public void Create(Movies movie)
       {
           db.Movies.Add(movie);
           db.SaveChanges();
       }

       public void Update(Movies movie)
       {
           db.Entry(movie).State = EntityState.Modified;
           db.SaveChanges();
       }

       public void Delete(Guid? id)
       {
           Movies movie = db.Movies.Find(id);
           if (movie != null)
           {
               db.Movies.Remove(movie);
               db.SaveChanges();
           }
       }
       public IEnumerable<Movies> FindItem(Func<Movies, Boolean> predicate)
       {
           return db.Movies.Where(predicate).ToList();
       }

       public int TotalPages(IEnumerable<Movies> query, int pageSize)
       {
            return (int)(Math.Ceiling(query.Count() / (decimal)pageSize));
        }

       public IEnumerable<Movies> GetPage(IEnumerable<Movies> query, int pageSize, int page)
       {
            return query.Skip((page - 1) * pageSize).Take(pageSize).ToList();
        }
       public void Dispose()
       {
           Dispose(true);
           GC.SuppressFinalize(this);
       }
       protected virtual void Dispose(bool disposing)
       {
            if (disposing)
            {
                db.Dispose();
            }
       }
    }
}