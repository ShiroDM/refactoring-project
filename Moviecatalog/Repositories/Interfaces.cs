﻿using Moviecatalog.Models;
using Moviecatalog.Models.Identity;
using System;
using System.Collections.Generic;

namespace Moviecatalog.Repositories
{
    public interface IMovieRepository : IDisposable
    {
        IEnumerable<Movies> GetAll();
        Movies Get(Guid? id);
        void Create(Movies item);
        void Update(Movies item);
        void Delete(Guid? id);
    }

    public interface IMoviePagesRepository : IMovieRepository
    {
        IEnumerable<Movies> FindItem(Func<Movies, Boolean> predicate);
        int TotalPages(IEnumerable<Movies> query, int pageSize);
        IEnumerable<Movies> GetPage(IEnumerable<Movies> query, int pageSize, int page);
    }

    public interface IApplicationUserRepository : IDisposable
    {
        IEnumerable<ApplicationUser> GetAll();
        ApplicationUser Get(string id);
    }
}