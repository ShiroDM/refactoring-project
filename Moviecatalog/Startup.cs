﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Moviecatalog.Startup))]
namespace Moviecatalog
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
