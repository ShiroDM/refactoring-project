﻿using Microsoft.AspNet.Identity;
using Moviecatalog.Models;
using Moviecatalog.Models.Identity;
using Moviecatalog.Repositories;
using Moviecatalog.ViewModels;
using Moviecatalog.ViewModels.MovieViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Moviecatalog.Controllers
{
    public class MoviesController : Controller
    {
        private MovieRepository movieRepository = new MovieRepository();
        private ApplicationUserRepository userRepository = new ApplicationUserRepository();
        private int pageSize = 5;

        public ActionResult Index(int page = 1)
        {
            if (Request == null || Request.HttpMethod == "GET")
            {
                string sort = "1";
                if (TempData["sort"] != null)
                    sort = TempData["sort"].ToString();

                IEnumerable<Movies> movies;
                switch (sort)
                {
                    case "2":
                        movies = movieRepository.GetPage(movieRepository.GetAll().OrderByDescending(movie => movie.title), pageSize, page);
                        break;
                    case "3":
                        movies = movieRepository.GetPage(movieRepository.GetAll().OrderBy(movie => movie.yearRelease), pageSize, page);
                        break;
                    case "4":
                        movies = movieRepository.GetPage(movieRepository.GetAll().OrderByDescending(movie => movie.yearRelease), pageSize, page);
                        break;
                    default:
                        movies = movieRepository.GetPage(movieRepository.GetAll().OrderBy(movie => movie.title), pageSize, page);
                        break;
                }

                IEnumerable<ApplicationUser> users = userRepository.GetAll();

                IEnumerable<MoviesView> moviesView = from movie in movies
                                                     join user in users on movie.userPosted equals user.Id
                                                     select new MoviesView
                                                     {
                                                         Id = movie.Id,
                                                         title = movie.title,
                                                         description = movie.description,
                                                         yearRelease = movie.yearRelease,
                                                         director = movie.director,
                                                         userPosted = movie.userPosted,
                                                         poster = movie.poster,
                                                         Nick = user.Nick
                                                     };

                int itemsTotal = movieRepository.GetAll().Count();
                int pagesTotal = (int)Math.Ceiling((decimal)(itemsTotal / pageSize)) + (itemsTotal % pageSize > 0 ? 1 : 0);
                MoviesViewModel moviesViewModel = new MoviesViewModel()
                {
                    pageInfo = new PageInfo(pageSize, pagesTotal),
                    moviesView = moviesView,
                };
                moviesViewModel.pageInfo.SetCurrentPageNumber(page);

                ViewBag.sort = sort;
                return View(moviesViewModel);
            }

            else if (Request.HttpMethod == "POST")
            {
                page = (page < 1 ? 1 : page);
                string sort = (Request.Form["sort"]);
                TempData["sort"] = sort;
                return RedirectToAction(null, null, new { page = page });
            }
            return View();
        }

        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movies movies = movieRepository.Get(id);
            if (movies == null)
            {
                return HttpNotFound();
            }
            return View(movies);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,title,description,yearRelease,director,userPosted,poster")] Movies movies, HttpPostedFileBase uploadImage)
        {

            if (ModelState.IsValid)
            {
                if (uploadImage != null)
                {
                    byte[] imageData = null;
                    using (var binaryReader = new BinaryReader(uploadImage.InputStream))
                    {
                        imageData = binaryReader.ReadBytes(uploadImage.ContentLength);
                    }
                    movies.poster = imageData;
                }
                movies.userPosted = User.Identity.GetUserId().ToString();
                movies.Id = Guid.NewGuid();
                movieRepository.Create(movies);
                return RedirectToAction("Index");
            }

            return View(movies);
        }

        // GET: Movies/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movies movies = movieRepository.Get(id);
            if (movies == null)
            {
                return HttpNotFound();
            }
            return View(movies);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,title,description,yearRelease,director,userPosted,poster")] Movies movies, HttpPostedFileBase uploadImage)
        {
            if (ModelState.IsValid)
            {
                if (uploadImage != null)
                {
                    byte[] imageData = null;
                    using (var binaryReader = new BinaryReader(uploadImage.InputStream))
                    {
                        imageData = binaryReader.ReadBytes(uploadImage.ContentLength);
                    }
                    movies.poster = imageData;
                }
                else
                {
                    movies.poster = movieRepository.Get(movies.Id).poster;
                }
                movies.userPosted = User.Identity.GetUserId().ToString();
                movieRepository.Update(movies);
                return RedirectToAction("Index");
            }
            return View(movies);
        }

        // GET: Movies/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movies movies = movieRepository.Get(id);
            if (movies == null)
            {
                return HttpNotFound();
            }
            return View(movies);
        }

        // POST: Movies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            movieRepository.Delete(id);
            return RedirectToAction("Index");
        }
        public ActionResult About()
        {
            ViewBag.Message = "Решение тестовго задания на вакансию Asp.Net-разработчик.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "E-mail: andry122003@list.ru";

            return View();
        }

        public ActionResult GetPoster(Guid? id)
        {
            Movies movie = movieRepository.Get(id);
            if (movie.poster != null)
                return File(movie.poster, "image/gif");
            else
                return File(new byte[] { }, "image/gif");
        }
    }
}
