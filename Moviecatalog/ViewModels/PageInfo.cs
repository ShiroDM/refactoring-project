﻿using System;

namespace Moviecatalog.ViewModels
{
    public class PageInfo : IPageInfo
    {
        public int pageSize_; // кол-во объектов на странице
        public int pagesTotal_; // всего страниц
        private int pageCurrentNumber_; // номер текущей страницы
        public PageInfo(int pageSize, int pagesTotal)
        {
            pageSize_ = pageSize;
            pagesTotal_ = pagesTotal;
        }
        public void SetCurrentPageNumber(int pageCurrentNumber)
        {
            pageCurrentNumber_ = pageCurrentNumber;
        }
        public int GetCurrentPageNumber()
        {
            return pageCurrentNumber_;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }

    }
}