﻿using System;

namespace Moviecatalog.ViewModels
{
    public interface IPageInfo : IDisposable
    {
        void SetCurrentPageNumber(int pageCurrentNumber);
        int GetCurrentPageNumber();
    }
}