﻿using System;
using System.Drawing;
using System.IO;
using System.Text;
using System.Web.Mvc;



namespace Moviecatalog.ViewModels.MovieViewModel
{
    public static class MoviePosterHelper
    {
        public static MvcHtmlString PosterHelper(this HtmlHelper html, byte[] poster)
        {
            StringBuilder result = new StringBuilder();
            //-- *********** Постер
            TagBuilder tag = new TagBuilder("div");
            TagBuilder tag2 = new TagBuilder("img");
            tag2.AddCssClass("img-responsive img-rounded");
            if (poster != null)
            {
                Image img = Image.FromStream(new MemoryStream(poster));
                int newWidth = 250;
                int newHeight = (int)Math.Ceiling((decimal)(newWidth * img.Height / img.Width));
                Bitmap bimg = new Bitmap(img, newWidth, newHeight);

                string s = String.Format("data:image/gif;base64,{0}", Convert.ToBase64String(ImageToByte((Image)bimg), Base64FormattingOptions.InsertLineBreaks));
                tag2.MergeAttribute("src", s);
            }
            else
            {
                tag2.MergeAttribute("src", "");
            }

            tag2.MergeAttribute("alt", "постер не загружен");
            tag2.MergeAttribute("id", "uploadImageShowVal");
            tag2.MergeAttribute("name", "uploadImageShowVal");
            //tag2.MergeAttribute("style", "width:320px; height:240px;");
            //tag2.MergeAttribute("onclick", "MyPopup(this)");
            tag.InnerHtml += tag2.ToString();
            result.Append(tag.ToString());
            return MvcHtmlString.Create(result.ToString());
        }
        private static byte[] ImageToByte(Image img)
        {
            using (var stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }
    }
}