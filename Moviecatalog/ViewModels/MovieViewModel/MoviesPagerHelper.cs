﻿using System;
using System.Text;
using System.Web.Mvc;


namespace Moviecatalog.ViewModels.MovieViewModel
{
    public static class MoviesPagerHelper
    {
        public static MvcHtmlString GetPageLinks(this HtmlHelper html, PageInfo pageInfo, Func<int, string> pageUrl, bool isUserAuthenticated, bool drowAdd)
        {
            int pageCount = 3;
            StringBuilder result = new StringBuilder();
            TagBuilder tag;
            if (pageInfo.GetCurrentPageNumber() >= pageCount + 2)
            {
                //1-я
                tag = new TagBuilder("a");
                tag.MergeAttribute("onclick", "LoadingShow()");
                tag.MergeAttribute("href", pageUrl(1));
                tag.InnerHtml = "1...";
                tag.AddCssClass("btn btn-default");
                result.Append(tag.ToString());
            }
            //Предидущая ?!!
            tag = new TagBuilder("a");
            tag.MergeAttribute("onclick", "LoadingShow()");
            tag.MergeAttribute("href", pageUrl((pageInfo.GetCurrentPageNumber() <= 1 ? 1 : pageInfo.GetCurrentPageNumber() - 1)));
            tag.InnerHtml = "<<";
            tag.AddCssClass("btn btn-default");
            result.Append(tag.ToString());

            for (int i = 1; i <= pageInfo.pagesTotal_; i++)
            {
                if (PageTagDrow(i, pageInfo, pageCount))
                {
                    tag = new TagBuilder("a");
                    tag.MergeAttribute("onclick", "LoadingShow()");
                    tag.MergeAttribute("href", pageUrl(i));
                    tag.InnerHtml = i.ToString();
                    // если текущая страница, то выделяем ее, например, добавляя класс
                    if (i == pageInfo.GetCurrentPageNumber())
                    {
                        tag.AddCssClass("selected");
                        tag.AddCssClass("btn btn-primary");
                    }
                    else
                    {
                        tag.AddCssClass("btn btn-default");
                    }
                    
                    result.Append(tag.ToString());
                }
            }
            //Следующая ?!!
            tag = new TagBuilder("a");
            tag.MergeAttribute("onclick", "LoadingShow()");
            tag.MergeAttribute("href", pageUrl((pageInfo.GetCurrentPageNumber() >= pageInfo.pagesTotal_ ? (pageInfo.pagesTotal_ == 0 ? 1 : pageInfo.pagesTotal_) : pageInfo.GetCurrentPageNumber() + 1)));
            tag.InnerHtml = ">>";
            tag.AddCssClass("btn btn-default");
            result.Append(tag.ToString());

            if (pageInfo.GetCurrentPageNumber() <= pageInfo.pagesTotal_ - (pageCount + 2))
            {

                //Последняя
                tag = new TagBuilder("a");
                tag.MergeAttribute("onclick", "LoadingShow()");
                tag.MergeAttribute("href", pageUrl(pageInfo.pagesTotal_ == 0 ? 1 : pageInfo.pagesTotal_));
                tag.InnerHtml = "..." + (pageInfo.pagesTotal_ > 0 ? pageInfo.pagesTotal_ : 1).ToString();
                tag.AddCssClass("btn btn-default");
                result.Append(tag.ToString());
            }

            if (drowAdd)
            {
                tag = new TagBuilder("a");
                if (!isUserAuthenticated)
                {
                    tag.MergeAttribute("disabled", "disabled");
                }
                else
                {
                    tag.MergeAttribute("onclick", "LoadingShow()");
                    tag.MergeAttribute("href", "/Movies/Create");
                }

                tag.InnerHtml = "Добавить фильм";
                tag.AddCssClass("selected");
                tag.AddCssClass("btn btn-primary");
                result.Append(tag.ToString());
            }

            return MvcHtmlString.Create(result.ToString());
        }
        private static bool PageTagDrow(int i, PageInfo pageInfo, int pageCount)
        {
            bool b = false;
            //Рисуем кратно pageCount
            //Нижняя граница
            int iMin = ((int)Math.Ceiling((decimal)(pageInfo.GetCurrentPageNumber()) / pageCount) - 1) * pageCount + 1;
            //Верхняя граница
            int iMax = ((int)Math.Ceiling((decimal)(pageInfo.GetCurrentPageNumber()) / pageCount)) * pageCount;
            //Ограничения
            if (iMax >= pageInfo.pagesTotal_)
            {
                iMax = pageInfo.pagesTotal_;
                if (iMin == iMax)
                {
                    iMin = iMin - pageCount;
                    if (iMin < 1)
                        iMin = 1;
                }
            }

            if (i >= iMin - 1 && i <= iMax + (iMin == 1 ? 2 : 1))
                b = true;

            return b;
        }
    }
}