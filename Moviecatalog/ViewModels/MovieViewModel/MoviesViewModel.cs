﻿using Moviecatalog.Models;
using System.Collections.Generic;

namespace Moviecatalog.ViewModels.MovieViewModel
{
    public class MoviesView : Movies
    {
        public string Nick { get; set; }
    }
    public class MoviesViewModel
    {
        public IEnumerable<MoviesView> moviesView { get; set; }
        public PageInfo pageInfo { get; set; }
    }
}