﻿using System;
using System.Drawing;
using System.IO;
using System.Text;
using System.Web.Mvc;


namespace Moviecatalog.ViewModels.MovieViewModel
{
    public static class MoviesRowHelper
    {
        public static MvcHtmlString RowHelper(this HtmlHelper html, MoviesView item, bool isUserAuthenticated, string userId)
        {
            StringBuilder result = new StringBuilder();
            //-- *********** Название
            TagBuilder tag = new TagBuilder("div")
            {
                InnerHtml = item.title
            };
            tag.AddCssClass("col-xs-12 col-sm-4 col-md-2 col-lg-2 ava");
            result.Append(tag.ToString());
            //-- *********** Постер
            tag = new TagBuilder("div");
            tag.AddCssClass("col-xs-12 col-sm-4 col-md-2 col-lg-1 ava-img");
            TagBuilder tag2 = new TagBuilder("img");
            tag2.AddCssClass("img-responsive img-rounded");
            if (item.poster != null)
            {
                Image img = Image.FromStream(new MemoryStream(item.poster));
                int newWidth = 100;
                int newHeight = (int) Math.Ceiling((decimal) (newWidth * img.Height / img.Width));
                Bitmap bimg = new Bitmap(img, newWidth, newHeight);

                string s = String.Format("data:image/gif;base64,{0}", Convert.ToBase64String(ImageToByte((Image)bimg), Base64FormattingOptions.InsertLineBreaks));
                tag2.MergeAttribute("src", s);
            }
            else
            {
                tag2.MergeAttribute("src", "");
            }
            tag2.MergeAttribute("alt", "постер не загружен");
            tag2.MergeAttribute("id", item.Id.ToString());
            //tag2.MergeAttribute("onclick", "MyPopup(this)");
            tag.InnerHtml += tag2.ToString();
            result.Append(tag.ToString());
            //-- *********** Описание
            tag = new TagBuilder("div");
            tag.MergeAttribute("word-wrap", "break-word"); 
            tag.InnerHtml = item.description; ;
            tag.AddCssClass("col-xs-12 col-sm-4 col-md-2 col-lg-4 ava");
            result.Append(tag.ToString());
            //-- *********** Год
            tag = new TagBuilder("div")
            {
                InnerHtml = item.yearRelease.ToString()
            };
            tag.AddCssClass("col-xs-12 col-sm-4 col-md-2 col-lg-1 ava");
            result.Append(tag.ToString());
            //-- *********** Режиссер
            tag = new TagBuilder("div")
            {
                InnerHtml = item.director
            };
            tag.AddCssClass("col-xs-12 col-sm-4 col-md-2 col-lg-1 ava");
            result.Append(tag.ToString());
            //-- *********** Ник
            tag = new TagBuilder("div")
            {
                InnerHtml = item.Nick
            };
            tag.AddCssClass("col-xs-12 col-sm-4 col-md-2 col-lg-1 ava");
            result.Append(tag.ToString());
            //-- *********** Инструменты
            tag = new TagBuilder("div");
            tag.AddCssClass("col-xs-12 col-sm-4 col-md-2 col-lg-1 ava");
            
            TagBuilder tag3 = new TagBuilder("div");

            //Подробно
            tag2 = new TagBuilder("a");
            tag2.MergeAttribute("href", @"/Movies/Details/" + item.Id.ToString());
            TagBuilder tag4 = new TagBuilder("img");
            tag4.AddCssClass("img-responsive img-icon");
            tag4.MergeAttribute("src", "/Pictures/details.png");
            tag4.MergeAttribute("alt", "Подробно");

            tag4.MergeAttribute("data-toggle", "tooltip");
            tag4.MergeAttribute("data-placement", "top");
            tag4.MergeAttribute("title", "Подробно.");

            tag2.InnerHtml += tag4.ToString();

            tag3.InnerHtml += tag2.ToString();

            
            if (item.userPosted == userId)
            {
                //Редактировать
                tag2 = new TagBuilder("a");
                tag2.MergeAttribute("href", "/Movies/Edit/" + item.Id.ToString());
                tag4 = new TagBuilder("img");
                tag4.AddCssClass("img-responsive img-icon");
                tag4.MergeAttribute("src", "/Pictures/edit.png");
                tag4.MergeAttribute("alt", "Редактировать");
                
                tag4.MergeAttribute("data-toggle", "tooltip");
                tag4.MergeAttribute("data-placement", "top");
                tag4.MergeAttribute("title", "Редактировать.");
                tag2.InnerHtml += tag4.ToString();
                //Удалить
                tag3.InnerHtml += tag2.ToString();
                tag2 = new TagBuilder("a");
                tag2.MergeAttribute("href", @"/Movies/Delete/" + item.Id.ToString());
                tag4 = new TagBuilder("img");
                tag4.AddCssClass("img-responsive img-icon");
                tag4.MergeAttribute("src", "/Pictures/Delete.png");
                tag4.MergeAttribute("alt", "Удалить");

                tag4.MergeAttribute("data-toggle", "tooltip");
                tag4.MergeAttribute("data-placement", "top");
                tag4.MergeAttribute("title", "Удалить.");
                tag2.InnerHtml += tag4.ToString();

                tag3.InnerHtml += tag2.ToString();
            }

            tag.InnerHtml += tag3.ToString();
            
            if (!isUserAuthenticated)
            {
                tag.MergeAttribute("data-toggle", "tooltip");
                tag.MergeAttribute("data-placement", "top");
                tag.MergeAttribute("title", "Для добавление фильмов в каталог необходимо войти или зарегистрироваться.");
            }
            
            result.Append(tag.ToString());
            //-- ***********
            
            //Перед блоком sm, который должен начинаться с новой строки-- >
            tag = new TagBuilder("div");
            tag.AddCssClass("clearfix");
            result.Append(tag.ToString());

            return MvcHtmlString.Create(result.ToString());
        }
        private static byte[] ImageToByte(Image img)
        {
            using (var stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }
    }
}