﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moviecatalog;
using Moviecatalog.Controllers;
using Moviecatalog.Models.Identity;

namespace UnitTestProject1
{
    [TestClass]
    public class AccountControllerTest
    {
        //Test (1) for method Login() in line 66 of AccountController
        [TestMethod]
        public void LoginTest1()
        {
            AccountController accountController = new AccountController();
            LoginViewModel loginViewModel = new LoginViewModel();

            loginViewModel.Email = "random@random.com";
            loginViewModel.Password = "ddQe455f.*_  =)";
            loginViewModel.RememberMe = false;
            string returlUrl = "random";
            var result = accountController.Login(loginViewModel, returlUrl);

            Assert.IsNotNull(result);
        }

        //Test (2) for method Login() in line 66 of AccountController
        [TestMethod]
        public void LoginTest2()
        {
            AccountController accountController = new AccountController();
            LoginViewModel loginViewModel = new LoginViewModel();

            loginViewModel.Email = "wrong email format";
            loginViewModel.Password = "10";
            loginViewModel.RememberMe = true;
            string returlUrl = " ";
            var result = accountController.Login(loginViewModel, returlUrl);

            Assert.IsNotNull(result);
        }

        //Test (3) for method Login() in line 66 of AccountController
        [TestMethod]
        public void LoginTest3()
        {
            AccountController accountController = new AccountController();
            LoginViewModel loginViewModel = new LoginViewModel();

            loginViewModel.Email = null;
            loginViewModel.Password = null;
            string returlUrl = null;
            var result = accountController.Login(loginViewModel, returlUrl);

            Assert.IsNotNull(result);
        }

        //Test (4) for method Login() in line 66 of AccountController
        [TestMethod]
        public void LoginTest4()
        {
            AccountController accountController = new AccountController();
            LoginViewModel loginViewModel = null;

            var result = accountController.Login(loginViewModel, null);

            Assert.IsNotNull(result);
        }

        //Test (1) for method VerifyCode in line 109 of AccountController
        [TestMethod]
        public void VerifyCodeTest1()   
        {
            AccountController accountController = new AccountController();
            VerifyCodeViewModel verifyCodeViewModel = new VerifyCodeViewModel();

            verifyCodeViewModel.Code = "random string";
            verifyCodeViewModel.Provider = "random string";
            verifyCodeViewModel.ReturnUrl = "random string";
            verifyCodeViewModel.RememberBrowser = true;
            verifyCodeViewModel.RememberMe = false;
            var result = accountController.VerifyCode(verifyCodeViewModel); 

            Assert.IsNotNull(result);
        }

        //Test (2) for method VerifyCode in line 109 of AccountController
        [TestMethod]
        public void VerifyCodeTest2()
        {
            AccountController accountController = new AccountController();
            VerifyCodeViewModel verifyCodeViewModel = new VerifyCodeViewModel();

            verifyCodeViewModel.Code = null;
            verifyCodeViewModel.Provider = null;
            verifyCodeViewModel.ReturnUrl = null;

            var result = accountController.VerifyCode(verifyCodeViewModel);
            Assert.IsNotNull(result);
        }

        //Test (3) for method VerifyCode in line 109 of AccountController
        [TestMethod]
        public void VerifyCodeTest3()
        {
            AccountController accountController = new AccountController();

            VerifyCodeViewModel verifyCodeViewModel = null;

            var result = accountController.VerifyCode(verifyCodeViewModel);
            Assert.IsNotNull(result);
        }

        //Test (1) for method ExternalLoginCallback in line 320 of AccountController
        [TestMethod]
        public void ExternalLoginCallbackTest1()
        {
            AccountController accountController = new AccountController();

            string returlUrl = "random";
            var result = accountController.ExternalLoginCallback(returlUrl);

            Assert.IsNotNull(result);
        }

        //Test (2) for method ExternalLoginCallback in line 320 of AccountController
        [TestMethod]
        public void ExternalLoginCallbackTest2()
        {
            AccountController accountController = new AccountController();

            string returlUrl = null;
            var result = accountController.ExternalLoginCallback(returlUrl);

            Assert.IsNotNull(result);
        }
    }
}

