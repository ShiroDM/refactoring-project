﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moviecatalog;
using Moviecatalog.Controllers;
using System;
using Moviecatalog.Models;
using System.Web;
using System.IO;
using System.Security.Claims;
using Moq;
using System.Security.Principal;

namespace UnitTestProject1
{
    [TestClass]
    public class MoviesControllerTest
    {
        public Func<string> GetUserId; //For testing
        //Test (1) for method Index() in line 24 of MoviesController
        [TestMethod]
        public void IndexTest1()
        {
            MoviesController moviesController = new MoviesController();
            ViewResult result = moviesController.Index(1) as ViewResult;

            Assert.IsNotNull(result);
        }

        //Test (2) for method Index() in line 24 of MoviesController
        [TestMethod]
        public void IndexTest2()
        {
            MoviesController moviesController = new MoviesController();
            ViewResult result = moviesController.Index(66) as ViewResult;

            Assert.IsNotNull(result);
        }

        //Test (1) for method Details() in line 87 of MoviesController
        [TestMethod]
        public void DetailsTest1()
        {
            MoviesController moviesController = new MoviesController();

            Guid testGuid = Guid.NewGuid();
            testGuid = Guid.Parse("8af47954-d0df-4e01-af62-b86a76892dc8"); //Witcher movie Guid
            ViewResult result = moviesController.Details(testGuid) as ViewResult;

            Assert.IsNotNull(result);
        }

        //Test (2) for method Details() in line 87 of MoviesController
        [TestMethod]
        public void DetailsTest2()
        {
            MoviesController moviesController = new MoviesController();

            Guid testGuid = Guid.NewGuid();
            testGuid = Guid.Parse("a3403ed7-0c57-4045-a304-d71f28144bba"); //Matrix movie Guid
            ViewResult result = moviesController.Details(testGuid) as ViewResult;

            Assert.IsNotNull(result);
        }

        //To fix faking movies.userPosted. Test not working.
        [TestMethod]
        public void EditTest1()
        {
            HttpContext.Current = new HttpContext(new HttpRequest(null, "http://tempuri.org", null), new HttpResponse(null));
            MoviesController moviesController = new MoviesController();
            //Fake Movie info
            Random rnd = new Random();
            Movies movie = new Movies();
            movie.title = "Movie: The Movie";
            movie.description = "This is a movie description";
            movie.director = "Fake Quentin Tarantino";
            movie.poster = new byte[10];
            movie.userPosted = "Random user";
            movie.yearRelease = 2035;
            movie.Id = Guid.Parse("5757d538-0c58-4bd5-88b0-12c6287caac4"); //random Guid
            rnd.NextBytes(movie.poster);
            byte[] byteBuffer = new Byte[10];
            rnd.NextBytes(byteBuffer);
            System.IO.MemoryStream testStream = new System.IO.MemoryStream(byteBuffer);
            MyTestPostedFileBase testfile = new MyTestPostedFileBase(testStream, "test/content", "test-file.png");

            ViewResult result = moviesController.Create(movie, testfile) as ViewResult;

            Assert.IsNotNull(result);


        }

        class MyTestPostedFileBase : HttpPostedFileBase
        {
            Stream stream;
            string contentType;
            string fileName;

            public MyTestPostedFileBase(Stream stream, string contentType, string fileName)
            {
                this.stream = stream;
                this.contentType = contentType;
                this.fileName = fileName;
            }

            public override int ContentLength
            {
                get { return (int)stream.Length; }
            }

            public override string ContentType
            {
                get { return contentType; }
            }

            public override string FileName
            {
                get { return fileName; }
            }

            public override Stream InputStream
            {
                get { return stream; }
            }

            public override void SaveAs(string filename)
            {
                throw new NotImplementedException();
            }
        }
    }
}

